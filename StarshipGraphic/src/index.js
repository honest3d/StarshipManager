import React from "react"
import ReactDOM from "react-dom"
import NCGStore from './stores/NodecgStore'
import { replicate } from './stores/NodecgStore'
import Background from './components/background.jsx';
import CircularIndicator from "./components/indicators/circular";

import { ThemeProvider } from "styled-components";
import { redAlert, normal } from "./theme";
import { StatusDialog } from "./components/status-dialog";
import { ShipImage } from "./components/ship";

class App extends React.Component {
  constructor() {
  super()
    this.state = {
      replicants: NCGStore.getReplicants(),
    }
  }

  componentDidMount() {
    // Subscribing to replicant changes
    replicate("shipColors")
    replicate("shipStateDialog")
    // We keep all our subscribed replicants in a single "replicants" object
    NCGStore.on("change", () => {
      this.setState({
        replicants: NCGStore.getReplicants(),
      })
    })
  }

  render() {
    const shipColors = this.state.replicants.shipColors
    return (
      <main>
        <ThemeProvider theme={shipColors=="redAlert" ? redAlert : normal}>
          <ShipImage />
          <StatusDialog topText={this.state.replicants.shipStateDialog.topText} mainText={this.state.replicants.shipStateDialog.mainText}/>
        </ThemeProvider>
      </main>
    )
  }
}

const root = document.getElementById("app")
ReactDOM.render(<App/>, root)