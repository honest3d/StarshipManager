const theme = {
    borderRadius: "20px",
    colors: {
        red: '#a71d31',
        orange: '#fa8334',
        green: '#00ff8b'
    }
}

const redAlert = {
    ...theme,
    colors:{
        ...theme.colors,
        primary: theme.colors.red
    }
}
const normal = {
    ...theme,
    colors:{
        ...theme.colors,
        primary: theme.colors.green
    }
}

export {theme, redAlert, normal};