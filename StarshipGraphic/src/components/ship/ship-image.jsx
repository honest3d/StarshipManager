import styled from 'styled-components';
import { Ship } from './ship';

const ShipContainer = styled.div`
    position:absolute;
    width:100vw;
    height:80vh;
    z-index:-1000;
    top:10vh;
    display:flex;
    justify-content:center;
`

const ShipImage = (props) => {

    return (
        <ShipContainer>
            <Ship />
        </ShipContainer>
    )
}

export { ShipImage }