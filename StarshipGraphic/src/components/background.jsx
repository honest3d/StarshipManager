import React from "react";

import * as styles from './background.module.css';

function Background(props) {
    return(
        <img className={styles.backgroundImage} src={props.image} />
    );
}

export default Background;