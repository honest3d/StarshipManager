import React from "react";

import { CircularProgressbarWithChildren, buildStyles } from "react-circular-progressbar";
import {AiOutlineFire} from 'react-icons/ai'
import 'react-circular-progressbar/dist/styles.css';
import { IconContext } from "react-icons";
import '../global.css';

function CircularIndicator(props) {
    return(
        <div style={{
            width: '50px'
        }}>
            <IconContext.Provider value={{color:'white',className:'glow'}}>
                <CircularProgressbarWithChildren
                    value={props.value}
                    circleRatio={0.75}
                    strokeWidth={15}
                    styles={buildStyles({
                        rotation: 1 / 2 + 1 / 8,
                        
                        trailColor: "#00000022"


                    })}
                >
                    <AiOutlineFire />
                </CircularProgressbarWithChildren>
            </IconContext.Provider>
        </div>
    )
    
}

export default CircularIndicator;