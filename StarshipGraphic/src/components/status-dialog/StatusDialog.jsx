import styled from 'styled-components';
const DialogOuter = styled.div`
    width: 100vw;
    height: 100vh;
    display: flex;
    align-items: center;
    justify-content: center;
    position: fixed;
    top: 0;
    left: 0;
`
const DialogFrame = styled.div`
    width: 75vw;
    height:150px;

    color: ${props => props.theme.colors.primary};
    background: #000;
    
    border-radius: ${props=>props.theme.borderRadius};
    border: 4px solid ${props => props.theme.colors.primary};

    display: grid;
    grid-template-rows: auto auto;
    justify-items: center;
    transition: all linear .25s;
`

const Subhead = styled.h2`
    font-family: ELIXIA;
    font-size:40px;
    font-weight: 600;
    letter-spacing: 5px;

    align-self: end;
    padding:0;
    margin:0
`
const Head = styled.h1`
    font-family: blanka;
    font-size:50px;
    letter-spacing:3px;

    align-self: beginning;
    padding:0;
    margin:0;
`

const StatusDialog = (props) => {
    return (
        <DialogOuter>
            <DialogFrame>
                <Subhead>
                    {props.topText.toUpperCase()}
                </Subhead>
                <Head>
                    {props.mainText.toUpperCase()}
                </Head>
            </DialogFrame>
        </DialogOuter>
    )
}

export { StatusDialog };