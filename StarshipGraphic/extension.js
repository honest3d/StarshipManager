'use strict'

// In this example replicants are being declared in the extension first
// You can always declare them in panels or graphics

module.exports = nodecg => {
	const timeReplicant = nodecg.Replicant('indicator', {defaultValue: 50});
	const nameReplicant = nodecg.Replicant('name', {defaultValue: "fellow reactive bundle craftsman"});
	const shipStatus = nodecg.Replicant('shipStateDialog', {defaultValue: {
		topText: "",
		mainText: "",
	}})
	const shipColors = nodecg.Replicant('shipColors', {defaultValue:"normal"})
}
